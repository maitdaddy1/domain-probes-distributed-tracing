# Domain Probes - Distributed Tracing

- Distributed trace via Domain Probes. -
 high-level instrumentation API that is oriented around domain semantics, encapsulating the low-level instrumentation plumbing required to achieve Domain Oriented Observability